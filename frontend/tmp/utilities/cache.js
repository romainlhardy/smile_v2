//abstraction layer for client persistence

(function (app) {
    //stores data in local memory
    //on load, newer data is stored in local storage
    var storage = {};
    // time resolution in minutes
    var EXPIRY_UNITS = 60 * 1000;
    var persistMins = 60;

    function currentTime() {
        return Math.floor((new Date().getTime()) / EXPIRY_UNITS);
    }

    app.cache = {
        set: function (key, value, options) {
            /*var options = options || {};
            var overwrite = options.overwrite === false ? false : true; //default true
            var persist = options.persist === false ? false : true; //default true
            var exists = !(this.get(overwrite) === undefined);

            //handle overwriting if overwrite is set to false
            if (exists && overwrite === false) {
                logger.error("app.localStorage: Overwrite = false, can't store '" + key + "'");
                return false;
            }

            if (exists == true) {
                storage[key].val = value;
                storage[key].iteration++;
                storage[key].time = currentTime();
            } else {
                storage[key] = {
                    val: value,
                    iteration: 0,
                    refreshRequested: false,
                    time: currentTime(),
                    persist: persist
                };
            }*/
            storage[key] = value;
            //persist
            if (value instanceof Backbone.Model) value = value.toJSON();
            if (value instanceof Backbone.Collection) value = value.toJSON();
            app.lscache.set(key, value, persistMins);
            return storage[key];
        },
        get: function (key, options) {
            /*
            if (storage[key]) {
                if (options && options.completeObj) {
                    return storage[key];
                } else {
                    return storage[key].val;
                }
            } else {
                return undefined;
            }*/
            if (!storage[key]) {
                storage[key] = app.lscache.get(key);
            }
            return storage[key]
        },
        persist: function (key) {
            if (storage[key]) {
                var val = storage[key];
                app.lscache.set(key, storage[key], persistMins);
                logger.info("persist " + key + " ==> " + storage[key].val);
                return storage[key];
            }
            return undefined;
        },
        persistAll: function() {
            
            
            _.each(storage, function(val, key) {
                if (storage[key].persist) {
                   this.persist(key);
                }
            }, this);
        },
        restore: function (key) {
            
            var val = app.lscache.get(key);
            if (val != undefined) {
                this.set(key, val, {persist: true});
            }
            return val;
        },
        restoreAll: function() {
            if (! window.localStorage) return;
            _.each(Object.keys(window.localStorage), function(val) {
                
               if (val.indexOf("lscache-" === 0) && val.indexOf("-cacheexpiration") === -1) {
                    this.restore(val.substring(8));
                }
            }, this);
        }
    }

})(app);