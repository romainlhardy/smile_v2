(function() {
    var UserStats = app.UserStats = app.UserStats || {};

    UserStats.Model = app.Model.extend({
        url: function() { return this.urlRoot + "user/getStats" },
        urlRoot: app.apiRoot
    });
})();