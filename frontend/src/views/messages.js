(function(app) {
    app.MessagesView = app.View.extend({
        initialize: function() {
            //only create template once, but ensure that creation takes place after app is loaded
            if (!this.template) app.MessagesView.prototype.template = this.template || _.template($('#messages-template').html());
        },

        render: function() {
            this.$el.html(this.template());
            logger.info("MessagesView rendered");

            return this;
        },

        remove: function() {
            logger.info("MessagesView removed");
            this.parentRemove();
        }
    })
})(app);