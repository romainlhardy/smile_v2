(function () {
    app.Router = Backbone.Router.extend({
        routes: {
            '': 'home',
            'home': 'home',
            'public': 'public',
            'groups': 'groups',
            'messages': 'messages',
            'profile': 'profile',
            'settings': 'settings',
            'about': 'about',
            'login': 'login',
            'register': 'register',
            'main': 'main',
            'logout': 'logout',
            'trophy_case': 'trophy_case',
            '*catchall': 'catchall'
        },

        initialize: function (options) {
            this.$app_container = $('#' + options.container);
            this.mainView = null;
        },

        home: function () {
            logger.info("router:home");
            this.setContentView({
                view: new app.HomeView(),
                header: {
                    type: 'menu'
                },
                loginWall: true,
                title: app.lang.get("home"),
                name: "home"
            });
        },

        trophy_case: function () {
            logger.info("router:trophy_case");
            this.setContentView({
                view: new app.TrophyCaseView(),
                header: {
                    type: 'menu',
                    back_href: '#home',
                    back_text: app.lang.get("home")
                },
                loginWall: true,
                title: app.lang.get("trophy_case"),
                name: "trophy_case"
            });
        },

        public: function () {
            logger.info("router:public");
            this.setContentView({
                view: new app.PublicView(),
                header: {
                    type: 'menu'
                },
                loginWall: true,
                title: app.lang.get("public"),
                name: "public"
            });
        },

        groups: function () {
            logger.info("router:groups");
            this.setContentView({
                view: new app.GroupsView(),
                header: (menu),
                loginWall: true,
                title: app.lang.get("groups"),
                name: "groups"
            });
        },

        messages: function () {
            logger.info("router:messages");
            this.setContentView({
                view: new app.MessagesView(),
                header: {
                    type: 'menu'
                },
                loginWall: true,
                title: app.lang.get("messages"),
                name: "messages"
            });
        },

        profile: function () {
            logger.info("router:profile");
            this.setContentView({
                view: new app.ProfileView(),
                header: {
                    type: 'menu'
                },
                loginWall: true,
                title: app.lang.get("profile"),
                name: "profile"
            });
        },

        settings: function () {
            logger.info("router:settings");
            this.setContentView({
                view: new app.SettingsView(),
                header: {
                    type: 'menu'
                },
                loginWall: true,
                title: app.lang.get("settings"),
                name: "settings"
            });
        },

        about: function () {
            logger.info("router:about");
            this.setContentView({
                view: new app.AboutView(),
                header: {
                    type: 'menu'
                },
                loginWall: true,
                title: app.lang.get("about"),
                name: "about"
            });
        },

        login: function () {
            logger.info("router:login");
            this.setContentView({
                view: new app.LoginView(),
                header: {
                    type: 'menu'
                },
                loginWall: false,
                header: {
                    type: 'logo'
                },
                title: app.lang.get("login"),
                name: "login"
            });
        },

        register: function () {
            logger.info("router:register");
            this.setContentView({
                view: new app.RegisterView(),
                header: {
                    type: 'menu'
                },
                loginWall: false,
                header: {
                    type: 'logo'
                },
                title: app.lang.get("register"),
                name: "register"
            });
        },

        logout: function () {
            logger.info("router:logger");
            var that = this;
            $.ajax({
                url: app.apiRoot + "auth/logout",

                complete: function (data) {
                    console.log(data);
                    that.setContentView({
                        view: new app.LogoutView(),
                        header: {
                            type: 'logo'
                        },
                        loginWall: false,
                        title: app.lang.get("logOut"),
                        name: "logout"
                    });
                }
            });
        },

        catchall: function () {
            logger.debug("catchall");
            //go to home view if undefined route is passed
            //and not currentView has not been set,
            //otherwise do nothing
            if (typeof(this.currentView) === 'undefined') {
                logger.debug(typeof(this.currentView));
                this.navigate("", {trigger: true, replace: true});
            }
        },
        setContentView: function (options) {
            options = options || {};
            var view = options.view,
                header = options.header,
                loginWall = options.loginWall,
                title = options.title,
                name = options.name;

            if (this.content && this.content.view) {
                this.content.view.remove();
            }

            if (loginWall && !app.getUser()) {
                this.navigate("login", {trigger: true, replace: true});
                return;
            }
            if (this.mainView === null) {
                this.mainView = new app.MainView({id: 'app-container'});
                this.mainView.setElement($('#app-container')).render({
                    footer: new app.FooterView()
                });
            }

            var views = {content: view};
            if (title) views.title = title;
            if (name) views.name = name;
            //views.name =
            //re-render header if need to show or hide navbar
            if (header.type == "menu") views.navbar = new app.NavbarView(header);
            else if (header.type == "logo") views.navbar = new app.LogoBarView();

            this.mainView.render(views);

            this.previousContent = this.content; //store previous view attributes, etc.
            this.content = options;

            if (options.navbtn) {
                $("#nav-home, #nav-public, #nav-groups, #nav-messages").removeClass("ui-btn-active ui-btn-down ui-btn-hover");
                $(options.navbtn).addClass("ui-btn-active");
                //trigger calls route function if true, replace doesn't update browser's history if true
            }
        }
    });
})();