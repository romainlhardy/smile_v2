(function(app) {
    app.settings = {
        menu_animation: {
            options:["push", "overlap", "reveal"],
                selection: 0
        },
        menu_fadeout: {
            selection: true
        },
        avatar_style: {
            options: ["avatar-rounded", "avatar-square"],
                selection: 1
        },
        menu_color_change: {
            selection: false
        },
        menu_instant_color_change: {
            selection: false
        },
        menu_animate_color: {
            selection: false
        },
        get: function(setting) {
            var selection =  this[setting].selection;
            if (this[setting].options) {
                return this[setting].options[selection];
            } else {
                return this[setting].selection;
            }
        },

        set: function(setting, value) {
            if (_.has(this, setting)) {
                var options = this[setting].options || {};
                var pos = _.indexOf(options, value);
                if (pos !== -1) {
                    this[setting].selection = pos;
                } else if (typeof(value) === "number" || typeof(value) === 'boolean') {
                    this[setting].selection = value;
                }
            }
        }
    };

    //default settings
    app.settings.set("menu_animation", "reveal");
    app.settings.set("menu_fadeout", false);
    app.settings.set("menu_color_change", true);
    app.settings.set("menu_instant_color_change", false);
    app.settings.set("menu_animate_color", false);
    app.settings.set("avatar_style", "avatar-square");

})(app);