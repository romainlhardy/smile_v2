window.responseTests = function(callback) {
    state.response0 = {
        userId: state.user3.UUID,
        resourceId: state.resource0.UUID, //resource that response is in
        sessionId: state.session0.UUID,
        answerChoice: [ 2 ], //must be an array of integers, min length of 1
        secondsToAnswer: 12.3, //seconds, > 0, number
        rating: 3.2 //rating >= 1 and <= 5, can be decimal
    }
    state.response1 = {
        userId: state.user4.UUID,
        resourceId: state.resource0.UUID, //resource that response is in
        sessionId: state.session0.UUID,
        answerChoice: [ 1 ],
        secondsToAnswer: 11,
        rating: 4
    }
    state.response2 = {
        userId: state.user5.UUID,
        resourceId: state.resource0.UUID, //resource that response is in
        sessionId: state.session0.UUID,
        answerChoice: [ 3 ],
        secondsToAnswer: 10,
        rating: 5
    }
    state.response3 = {
        userId: state.user4.UUID,
        resourceId: state.resource1.UUID, //resource that response is in
        sessionId: state.session0.UUID,
        answerChoice: [ 0 ],
        secondsToAnswer: 4,
        rating: 3
    }
    state.response4 = {
        userId: state.user6.UUID,
        resourceId: state.resource1.UUID, //resource that response is in
        sessionId: state.session0.UUID,
        answerChoice: [ 2 ],
        secondsToAnswer: 5,
        rating: 2
    }
    state.response5 = {
        userId: state.user6.UUID,
        resourceId: state.resource1.UUID, //resource that response is in
        sessionId: state.session3.UUID,
        answerChoice: [ 2 ],
        secondsToAnswer: 5,
        rating: 2
    }

    createResponse0();
    function createResponse0() {
        var response = state.response5;
        servicesTest('create response0 - non-group member', 'response', 'POST', state.user10,
            response,
            false,
            "success",
            function (response) {
                equal(response.error && response.error.code, 40, "Correct error code returned");
                createResponse1();
            }
        );
    }

    function createResponse1() {
        var response = state.response5;
        servicesTest('create response0 - non-group member, activity owner', 'response', 'POST', state.user10,
            response,
            true,
            "success",
            function (response) {
                console.log(response);
            }
        );
    }

    function createInst1() {
        var institution = state.institution1;
        servicesTest('create institution1 - non-admin user', 'institution', 'POST', state.user7,
            _.omit(institution, 'UUID'),
            true,
            "success",
            function (response) {;
                if (response && response.UUID) institution.UUID = response.UUID;
                equal(_.has(response, 'UUID'), true, "UUID included in response");
                getInstByName();
            }
        );
    }

    function getInstByName() {
        var institution = state.institution0;
        servicesTest('institution getByName', 'institution/getByName/' + institution.name, 'GET', null,
            null, true, "success",
            function (response) {
                equal(institution.UUID, response.institution.UUID, "Correct UUID is fetched");
                updateInst();
            }
        );
    }
    function updateInst() {
        var institution = state.institution0;
        servicesTest('update institution', 'institution/' + institution.UUID, 'PUT', null,
            {
                institutionType: "NGO",
                website: "www.smilec.org",
                sector: "EdTech"
            }, true, "success",
            function (response) {
                getInst();
            }
        );
    }
    function getInst() {
        var institution = state.institution0;
        servicesTest('get institution', 'institution/' + institution.UUID, 'GET', null,
            null, true, "success",
            function (response) {
                equal(institution.UUID, response.institution.UUID, "Correct UUID is fetched");
                equal(response.institution.institutionType, "NGO", "Correct institutionType is fetched");
                equal(response.institution.website, "www.smilec.org", "Correct website is fetched");
                equal(response.institution.sector, "EdTech", "Correct sector is fetched (newly added field)");
                equal(response.institution.country, "United States", "Correct UUID is fetched - (old unchanged field)");
                getInstNamesFromUUID();
            }
        );
    }
    function getInstNamesFromUUID() {
        servicesTest('get institution names from UUIDs', 'institution/getNamesFromUUID', 'POST', null,
            {
                UUIDs: [ state.institution0.UUID, state.institution1.UUID ]
            }, true, "success",
            function (response) {
                var UUIDs = _.pluck(response.institutions, "UUID");
                var names = _.pluck(response.institutions, "name");
                equal(UUIDs.indexOf(state.institution0.UUID) > -1, true);
                equal(UUIDs.indexOf(state.institution0.UUID) > -1, true);
                equal(names.indexOf(state.institution0.name) > -1, true);
                equal(names.indexOf(state.institution0.name) > -1, true);
                instAutoFill();
            }
        );
    }
    function instAutoFill() {
        servicesTest('institution name auto fill', 'institution/nameAutoFill&name=' + 'Goth', 'GET', null,
            null, null, null,
            function (response) {
                var UUIDs = _.pluck(response.institutions, "UUID");
                var names = _.pluck(response.institutions, "name");
                equal(UUIDs.indexOf(state.institution1.UUID) > -1, true);
                equal(names.indexOf(state.institution1.name) > -1, true);
                callback && callback();
            }
        );
    }
}