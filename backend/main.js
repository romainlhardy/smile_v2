/**
 * Copyright (C) SMILE Consortium 2013,
 * Developed by Neticle Portugal
 */

// Requires Nitrous Framework
require('./nitrous/framework/nitrous.js');

//add underscore module, and make accessible via global var
var underscore = require("underscore");
global.underscore = underscore;
global._ = underscore;

//override console.log
var oldLog = console.log;

//hide console messages
console.log = function() {};
console.debug = function(msg) {
    oldLog.apply(console, arguments);
};

$.using(
	'system.http.server',
	'system.restful.service',
	'system.couch.database'
);

// Create the couch database handle
var couchDB = $.couchDatabase.create('http', '127.0.0.1', 5984, 'smile'/*, username, password */);

// Create the HttpServer instance to handle requests
var server = $.httpServer.create(1337);
var service = $.restfulService.create(server);
service.couchDB = couchDB;

// Register the service as the 'app' module and load it
service._id = 'app';
module.exports = service;
$.load('application.main');

// Load the application modules
$.using(

	// SMILE Nitrous extensions
	'application.modules.extensions.http.response',
	'application.modules.extensions.restful.context',
	'application.modules.extensions.validator',
	
	// Service request custom validators
	'application.modules.validators.input',
	'application.modules.validators.no-session',
	'application.modules.validators.session',
	'application.modules.validators.admin',

	// Request handling modules
	'application.modules.auth',
	'application.modules.user',
	'application.modules.institution',
	'application.modules.group',
	'application.modules.session',
	'application.modules.resource',
	'application.modules.response',
	'application.modules.activity'

);

// Start listening for requests
server.start();
