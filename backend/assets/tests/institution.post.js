$.ajax({

	url: '/institution',
	type: 'POST',
	dataType: 'json',
	contentType: 'application/json',
	
	data: JSON.stringify({
		name: "Stanford University",
		institutionType: "University",
		country: "United States",
		region: "California",
		city: "Stanford",
		address: "520 Galvez Mall",
		zip: "94305",
		creator: "asf8h22hnoejnf298hn0o23",
		createdOn: [2009, 1, 31, 18, 4, 11],
		website: "www.stanford.edu"
		// photoUrl: "/photos/user/as3gf2eg24g24g.png"
	})

});
