/* by_id */
function(d) {
	if(d.doctype === 'resource') {
		emit(d._id, d);
	}
}
/* by_activity */
function(doc) {
    if( doc.activityIds ) {
        for( var i=0, l=doc.activityIds.length; i<l; i++) {
            emit( doc.activityIds[i], doc );
        }
    }
}