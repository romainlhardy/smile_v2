# Smile global webservice


## Configuring database

Create a database named 'smile':

`$ curl -X PUT http://127.0.0.1:5984/smile`

Load the design documents located on the `couchdb/` directory:



## Running the service
`$ nodejs main.js`

Service will listen in port 1337 by default

curl -X PUT http://127.0.0.1:5984/smile/_design/activity --data-binary @./_design.activity.json
curl -X PUT http://127.0.0.1:5984/smile/_design/generic --data-binary @./_design.generic.json
curl -X PUT http://127.0.0.1:5984/smile/_design/group --data-binary @./_design.group.json
curl -X PUT http://127.0.0.1:5984/smile/_design/institution --data-binary @./_design.institution.json
curl -X PUT http://127.0.0.1:5984/smile/_design/resource --data-binary @./_design.resource.json
curl -X PUT http://127.0.0.1:5984/smile/_design/response --data-binary @./_design.response.json
curl -X PUT http://127.0.0.1:5984/smile/_design/session --data-binary @./_design.session.json
curl -X PUT http://127.0.0.1:5984/smile/_design/user --data-binary @./_design.user.json
