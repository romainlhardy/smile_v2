/**
 * Copyright (C) SMILE Consortium 2013,
 * Developed by Neticle Portugal
 */
 
$.using(
	
	'system.object',	
	'application.modules.schema.response'
	
);

/**
 * Sanitizes the document for display.
 *
 * @param object document
 *	The document to be sanitized.
 *
 * @return object
 *	The sanitized document.
 */
function sanitize_response_document(document) {

	// Define the photo URL
	if(document._attachments && document._attachments['photo']) {
		document.photoUrl = this.couchDB.baseUrl + '/' + document._id + '/photo';
	}
	
	return this.sanitize(document);

};

/**
 * Returns an error status, code and message based on the given
 * validation report.
 *
 * @param object report
 *	The validation report.
 *
 * @return mixed[]
 *	An array containing the response status, code and message.
 */
function build_validation_report(report) {
	
	// Build the error response
	var validators = report.error.byValidators;
	var code, message;
	
	if(validators.required) {
		
		code = 1;
		message = 'Missing required fields in request: ["' + validators.required.join('", "') + '"]';
		
	} else if(validators.unique) {
		
		if(validators.unique.indexOf('name') > -1) {
		
			code = 16;
			message = 'Response name already exists.';
			
		} else {
			code = 2;
			message = 'Unique value already exists: ["' +
				validators.unique.join('", "') + '"]';
		}
		
	} else {
	
		code = 2;
		message = 'Input validation error: ["' +
			report.error.attributes.join('", "') + '"]';
			
	}
	
	return [ 400, code, message ];
}

/**
 * Sends an error response according to the error report.
 *
 * @param object report
 *	The validation error report to build the response from.
 */
function send_validation_report(report) {

	// Send the error response
	var report = build_validation_report.call(this, report);
	this.response.sendError(report[0], report[1], report[2]);
	
}

/**
 * Performs the required validation by checking if the currently authenticated
 * user is a member or organizer of the given group.
 *
 * @param object group
 *	The group to be verified.
 *
 * @param boolean organizer
 *	A flag indicating wether or not organizer status is required.
 *
 * @param function callback
 *	A callback to handle event completion in the same context as applied to
 *	this function at the time of its call.
 */
function do_groupValidation(group, organizer, callback) {

	console.log('Response: validating group "' + group.name + '" ...');

	// Get the session data
	var session = this.session.data;
	var me = session.id_user;
	
	// Get the member UUID collection to compare
	var collection = organizer ? group.organizerIds : group.memberIds;

	// Determine whether or not the user is a member of the group
	var valid = (
		group.owner === me ||
		(collection && collection.indexOf(me) > -1)
	);

	// Validation completed
	callback.call(this, valid);
	
}

/**
 * Performs the required validation by checking if the currently authenticated
 * user is a member or organizer of the given groups.
 *
 * @param object[] groups
 *	The groups to be verified.
 *
 * @param boolean organizer
 *	A flag indicating wether or not organizer status is required.
 *
 * @param function callback
 *	A callback to handle event completion in the same context as applied to
 *	this function at the time of its call.
 */
function do_groupsValidation(groups, organizer, all, callback) {

	console.log('Response: validating groups...');

	var groupsCount = groups.length;
	var groupsProcessed = 0;
	var groupsValidated = 0;
	
	// Handle group validation result
	var do_handleGroupValidation = function(success) {
	
		++groupsProcessed;
		
		if(success) {
			++groupsValidated;
		}
		
		// Detect when all validations are complete
		if(groupsProcessed === groupsCount) {
			
			// Validation result
			var result = all ?
				(groupsValidated === groupsProcessed) : (groupsValidated > 0);
			
			callback.call(this, result);
			
		}
		
	}
	
	// Run validation for all groups
	for(var i in groups) {
		do_groupValidation.call(this, groups, organizer, do_groupsValidation);
	}

}

/**
 * Performs the required validation by checking if the currently authenticated
 * user is a member or organizer of the given activity owning group.
 *
 * @param object activity
 *	The activity to be verified.
 *
 * @param boolean organizer
 *	A flag indicating wether or not organizer status is required.
 *
 * @param function callback
 *	A callback to handle event completion in the same context as applied to
 *	this function at the time of its call.
 */
function do_activityGroupValidation(activity, organizer, callback) {

	console.log('Response: validating activity group...');
		
	// Get the activity owning group
	this.find('group', activity.owningGroup, this, function(group) {
		do_groupValidation.call(this, group, organizer, callback);
	});

}

/**
 * Performs the required validation by checking if the currently authenticated
 * user is a member or organizer of the given activity owning group.
 *
 * @param object activity
 *	The activity to be verified.
 *
 * @param boolean organizer
 *	A flag indicating wether or not organizer status is required.
 *
 * @param function callback
 *	A callback to handle event completion in the same context as applied to
 *	this function at the time of its call.
 */
function do_activityAccessValidation(activity, organizer, callback) {
	
	var session = this.session.data;
	var me = session.id_user;
	
	// Activity owners have access to the activity
	if(session.admin || me === activity.owner) {
		callback.call(this, true);
		return;
	}
	
	// Check all groups to see if the user is a member of it
	do_activityGroupValidation.call(this, activity, organizer, callback);
	
}

/**
 * Performs the required validation by checking if the currently authenticated
 * user is a member or organizer of the given activities owning group.
 *
 * @param object[] activities
 *	The activities to be verified.
 *
 * @param boolean organizer
 *	A flag indicating wether or not organizer status is required.
 *
 * @param function callback
 *	A callback to handle event completion in the same context as applied to
 *	this function at the time of its call.
 */
function do_activitiesGroupValidation(activities, organizer, callback) {

	console.log('Response: validating activities groups...');

	// Get the session data
	var session = this.session.data;
	var me = session.id_user;
	
	// Check counter
	var activitiesCount = activities.length;
	var activitiesProcessed = 0;
	var activitiesValidated = 0;
	
	// Handle activity validation results
	var do_handleActivityValidation = function(success) {
	
		++activitiesProcessed;
		
		if(success) {
			++activitiesValidated;
		}
		
		// Detect when all validations are complete
		if(activitiesProcessed === activitiesCount) {
			callback.call(this, (activitiesValidated === activitiesCount));
		}
	
	};

	for(var i in activities) {
	
		// Process each activity separatedly
		var activity = activities[i];
		
		// No further validations for activity owners
		if(activity.owner === me || !activity.owningGroup) {
			do_handleActivityValidation.call(this);
			continue;
		}
		
		// Do the activity group validation
		do_activityGroupValidation.call
			(this, activity, organizer, do_handleActivityValidation);
	
	}
	
}

/**
 * Performs the required validation by checking if the currently authenticated
 * user is a member or organizer of the given session activity owning group.
 *
 * @param object session
 *	The session to be verified.
 *
 * @param boolean organizer
 *	A flag indicating wether or not organizer status is required.
 *
 * @param function callback
 *	A callback to handle event completion in the same context as applied to
 *	this function at the time of its call.
 */
function run_sessionActivityGroupValidation(session, organizer, callback) {

	console.log('Response: validating resource.owningGroup property...');

	// Get the session activity
	this.find('activity', session.activity, this, function(activity) {
		
		do_activityGroupValidation.call(this, activity, organizer, function(success) {
		
			// Validation success
			if(success) {
				callback.call(this);
				return;
			}
			
			this.response.sendError(403, 40, 'Admin authorization required to set a response\'s session to a session with an activity not owned by you which has an owningGroup in which you are not a group member.');
		
		});
		
	});

}

/**
 * Performs the required validation by checking if the currently authenticated
 * user is a member or organizer of the given resource activity owning group.
 *
 * @param resource resource
 *	The resource to be verified.
 *
 * @param boolean organizer
 *	A flag indicating wether or not organizer status is required.
 *
 * @param function callback
 *	A callback to handle event completion in the same context as applied to
 *	this function at the time of its call.
 */
function run_resourceActivitiesGroupValidation(resource, organizer, callback) {

	console.log('Response: validating resource.activityIds property...');

	// Find all resource activities
	this.findAllEx('activity', 'by_id', { keys: resource.activityIds }, function(activities) {

		do_activitiesGroupValidation.call(this, activities, organizer, function(success) {
		
			// Validation success
			if(success) {
				callback.call(this);
				return;
			}
			
			this.response.sendError(403, 41, 'Admin authorization required to set a response\'s resource to a hidden resource (resource\'s visible value equals false) in which you are not the resource\'s owner or an organizer in the resource\'s owningGroup.');
		});
		
	});
}

/**
 * Performs the required resource visibility validation.
 *
 * @param resource resource
 *	The resource to be verified.
 *
 * @param function callback
 *	A callback to handle event completion in the same context as applied to
 *	this function at the time of its call.
 */
function run_resourceVisibilityValidation(resource, callback) {

	// Get the session data
	var session = this.session.data;
	var me = session.id_user;
	
	// No further checks for admins and resource owners
	if(resource.visible || session.admin || resource.owner === me) {
		callback.call(this);
		return;
	}
	
	// Find all resource activities and make sure I'm an organizer in at least one
	run_resourceActivitiesGroupValidation.call(this, resource, true, callback);

}

/**
 * Performs the required response session validation.
 *
 * @param resource resource
 *	The resource to be verified.
 *
 * @param function callback
 *	A callback to handle event completion in the same context as applied to
 *	this function at the time of its call.
 */
function run_responseSessionValidation(response, callback) {

	console.log('Response: validating response.session property...');

	// No session referenced, no further checks
	if(!response.sessionId) {
		callback.call(this);
		return;
	}
	console.debug(1.51);
	// Find the response session
	this.find('session', response.sessionId, this, function(session) {
	    console.debug(1.52);
		// Admin authorization required to set a response's session to a session
		// with an activity which has an owningGroup in which you are not a 
		// group member.
		run_sessionActivityGroupValidation.call(this, session, true, callback);
	    console.debug(1.53);
	});
	
	
}

/**
 * Performs the required response limit validation.
 *
 * @param resource resource
 *	The resource to be verified.
 *
 * @param function callback
 *	A callback to handle event completion in the same context as applied to
 *	this function at the time of its call.
 */
function run_resourceResponseLimitValidation(resource, response, callback) {

	// Make sure this response/resource combination is ellegible to skip
	// limit validations, including if responses '._id' is set, meaning it's
	// an update operation that won't result in the creation of a new 
	// document.
	if(response._id || !response.sessionId || 
		!resource.responseLimit || resource.responseLimit < 1) {
		callback.call(this);
		return;
	}

	// Find matching responses
	this.couchDB.get('response', 'by_resource_session', { key: [response.resourceId, response.sessionId] }, this, function(results) {	// Multiple responses not required.

		var count = (results.length);
		var limit = resource.responseLimit;
        if (!limit && limit !== 0) {
            limit = -1;
        }

        console.log('Response: validating response limit [' + count + ' / ' + limit + '] ...');

        if(!resource.allowMultipleResponses && count > 0) {
            this.response.sendError(403, 45, 'You cannot submit more responses to the same resource and session than permitted by the resource\'s responseLimit.');
            return;
        }

        // Make sure adding a new response wouldn't be over the line
		if(limit !== -1 && count + 1 > limit) {
			this.response.sendError(403, 45, 'You cannot submit more responses to the same resource and session than permitted by the resource\'s responseLimit.');
			return;
		}
		
		callback.call(this);
	
	});

}

/**
 * Performs the required response validation.
 *
 * @param resource resource
 *	The resource to be verified.
 *
 * @param function callback
 *	A callback to handle event completion in the same context as applied to
 *	this function at the time of its call.
 */
function run_responseValidation(response, callback) {

	// Get the session data
	var session = this.session.data;
	var me = session.id_user;

	// Session may not be referenced
	if(session.admin || !response.sessionId) {
		callback.call(this);
		return;
	}
    console.debug(1.1);
	// Find the referenced resourceId
	this.find('resource', response.resourceId, this, function(resource) {

		// You cannot update a response to a resource if the resource's 
		// allowResponseRevision is false.
		if(response._id && !resource.allowResponseRevision) {
			this.response.sendError(403, 47, 'You cannot update or delete a response to a resource if the resource\'s allowResponseRevision is false.');
			return;
		}
	    console.debug(1.2);
		// Responses can only be associated with a hidden resource if you are 
		// the resource's owner or an organizer in the response activities
		// owningGroup.

		run_resourceVisibilityValidation.call(this, resource, function() {
            console.debug(1.3);
			// You cannot submit a response to a resource in which you are the 
			// creator if the resource's allowCreatorResponse field is false.
			if(resource.creator === me && !resource.allowCreatorResponse) {
				this.response.sendError(403, 46, 'You cannot submit or modify a response to a resource in which you are the creator if the resource\'s allowCreatorResponse field is false.');
				return;
			}

			// Get the number of selected answers
			var selected = response.answerChoices ? 
				response.answerChoices.length : 0;
		
			if(resource.multiSelect) {
			
				// Get the selected and limit numbers
				var limit = resource.selectLimit;
		
				// You have exceed the selectLimit. The selectLimit for this 
				// response's resource is [x]. You have submitted [y] answerChoices.
				if(limit < selected) {
					return this.response.sendError(403, 43,
						'You have exceed the selectLimit. The selectLimit for this response\'s resource is ' +
						'[' + limit + ']. You have submitted [' + selected + '] answerChoices.'
					);
				}
			
			} else if (selected > 1) {
				
				// Prevent multiple choices when not allowed
				this.response.sendError(400, 2, 'Multiple choices selected for non-multiSelect resource.')
				return;
				
			}
            console.debug(1.4);
			// You cannot submit more responses to the same resource and session
			// than permitted by the resource's responseLimit.
			run_resourceResponseLimitValidation.call(this, resource, response, function() {
                console.debug(1.5);
				// Admin authorization required to set a response's session to a 
				// session with an activity which has an owningGroup in which 
				// you are not a group member.
				run_responseSessionValidation.call(this, response, callback);
			
			});
			
		});
	
	});

}

/**
 * Creates a new response.
 */
$.app.handle('POST', '/response', ['smile-session', 'smile-input'], function() {

	// Get the current session data
	var session = this.session.data;
	var me = session.id_user;

	// Get the input data
	var input = this.request.body;

	// Validate the user input data
	$.schemaResponse.validator.validate('POST', input, 
			this, function(report, input) {

	
		// Report any validation errors
		if(!report.success) {
			send_validation_report.call(this, report, input);
			return;
		}
		console.debug(1);
		// Perform response validation
		run_responseValidation.call(this, input, function() {
			console.log('Response: posting validated input...');
			console.log(input);
            console.debug(2);
			// Build the document and post it to couchdb
			var document = $.object.extend({}, $.schemaResponse.template, input);
			
			this.couchDB.store(document, this, function(success, id, revision) {
			
				if(success) {
					this.response.sendUUID(id);
				} else {
					this.response.sendError(500, -1, 'Unable to post to CouchDB.');
				}
			
			});
		});
	
	});

});

/**
 * Updates an existing response.
 */
$.app.handle('PUT', '/response/%w', ['smile-session', 'smile-input'], function(uuid) {

	// Get the current session data
	var session = this.session.data;
	var me = session.id_user;
	
	// Get the input data
	var input = this.request.body;
	
	// Validate the user input data
	$.schemaResponse.validator.validate('POST', input, 
			this, function(report, input) {
	
		// Report any validation errors
		if(!report.success) {
			send_validation_report.call(this, report, input);
			return;
		}
		
		// Find the response to edit
		this.find('response', uuid, this, function(response) {
		
			// You cannot update a response whose userId is from a different 
			// user unless you are an admin.
			if(!session.admin && response.userId !== me) {
				this.response.sendError(403, 48, 'You cannot update a response whose userId is from a different user unless you are an admin.');
				return;
			}
		
			// Perform response validation
			run_responseValidation.call(this, input, function() {
		
				// Create the final document
				var document = $.object.extend({}, $schema.template, response, input);
				
				this.couchDB.store(document, this, function(success, id) {
				
					if(success) {
						this.response.sendUUID(id);
					} else {
						this.response.sendError(500, -1, 'Unable to post to CouchDB.');
					}
				
				});
			});
		
		});
	
	});

});

/**
 * Deletes an existing response.
 */
$.app.handle('DELETE', '/response/%w', ['smile-session'], function(uuid) {

	// Session is required
	var session = this.session.data;
	var me = session.id_user;
	
	// Find the record to delete
	this.find('response', uuid, this, function(response) {
	
		// Perform the actual delete
		var do_delete = function() {
			this.couchDB.delete(response._id, response._rev, function(success) {
				if(success) {
					this.sendOK();
				} else {
					this.response.sendError(500, -1, 'Failed to post to CouchDB.');
				}
			});
		};
		
		// Admins can simply delete
		if(session.admin) {
			do_delete.call(this);
			return;
		}
		
		// You cannot delete a response whose userId is from a different user 
		// unless you are an admin.
		if(response.userId !== me) {
			this.response.sendError(403, 48, 'You cannot delete a response whose userId is from a different user unless you are an admin.');
			return;
		}
		
		// Find the response resource
		this.find('resource', response.resourceId, this, function(resource) {
		
			// Response revision is allowed, so deleting is OK.
			if(resource.allowResponseRevision) {
				do_delete.call(this);
				return;
			}
			
			this.response.sendError(403, 47, 'You cannot delete a response to a resource if the resource\'s allowResponseRevision is false, unless you are an admin.');
		
		});
	
	});
});

/**
 * Fetches an existing response.
 */
$.app.handle('GET', '/response/%w', ['smile-session'], function(uuid) {

	// Session is required
	var session = this.session.data;
	var me = session.id_user;
	
	// Find the requested response
	this.find('response', uuid, this, function(response) {
	
		// Sanitize the document for display
		var result = this.sanitize(response);
		
		// Send the document
		this.response.send(200, {
			success: true,
			response: result
		});
	
	});
	

});


/**
 * Fetches existing responses by user.
 */
$.app.handle('GET', '/response/userResponses/%w', ['smile-session'], function(uuid) {

	// Session is required
	var session = this.session.data;
	var me = session.id_user;
	
	// You cannot request the responses for a user other than the currently 
	// authenticated user unless you are an admin.
	if(!session.admin && me !== uuid) {
		this.response.sendError(403, 50, 'You cannot request the responses for a user other than the currently authenticated user unless you are an admin.');
		return;
	}
	
	// Get all responses by userId
	this.couchDB.get('response', 'by_userId', { key: uuid }, this, function(responses) {
	
		// Normalize all results
		var results = [];
		for (var i in responses) {
			results.push(this.sanitize(responses[i]));
		}
		
		// Send the responses
		this.response.send(200, {
			success: true,
			responses: results
		});
	
	});

});

/**
 * Fetches existing responses by session.
 */
$.app.handle('GET', '/response/sessionResponses/%w', ['smile-session'], function(uuid) {

	// Session is required
	var session = this.session.data;
	var me = session.id_user;
	
	// Get all responses by userId
	this.couchDB.get('response', 'by_sessionId', { key: uuid }, this, function(responses) {
	
		var resourceIds = query.resourceIds;
		
		// Split the resourceIds string into an array
		if(resourceIds) {
		
			resourceIds = resourceIds.split(',');
			
			// Trim all resource ids
			for(var i in resourceIds) {
				resourceIds[i] = $string.trim(resourceIds[i]);
			}
			
		}
		
		console.log('Response: filtering by resource ids...');
		console.log(resourceIds);
	
		// Normalize all results
		var results = [];
		for (var i in responses) {
		
			var response = responses[i];
		
			// Filter to just those that matter
			if(!resourceIds || resourceIds.indexOf(response.resourceId) > -1) {
				results.push(this.sanitize(response));
			}
			
		}
		
		// Send the responses
		this.response.send(200, {
			success: true,
			responses: results
		});
	
	});

});

/**
 * Fetches existing responses by session.
 */
$.app.handle('GET', '/response/activityResponses/%w', ['smile-session'], function(uuid) {

	// Session is required
	var session = this.session.data;
	var me = session.id_user;
	
	// Find the requested activity
	this.find('activity', uuid, this, function(activity) {
	
		// Check wether or not this user is the activity owner or a member
		// of the organizers array.
		do_activityAccessValidation.call(this, activity, true, function(success) {
		
			// User does not have access
			if(!success) {
				this.response.sendError(403, 51, 'Only organizers of a activity\'s owningGroup, admins, or the activity\'s owner can call this function.');
				return;
			}
			
			// Get all sessions by activity
			this.couchDB.get('session', 'by_activity', { key: activity._id }, this, function(sessions) {
			
				var sessionIds = [];
			
				// Get all session ids
				for(var i in sessions) {
					sessionIds.push(sessions[i]._id);
				}
				
				// Get all responses by session id
				this.couchDB.get('response', 'by_sessionId', { keys: sessionIds }, this, function(responses) {
				
					// Normalize all results
					var results = [];
					for (var i in responses) {
					
						var response = responses[i];
						results.push(this.sanitize(response));
						
					}
					
					// Send the responses
					this.response.send(200, {
						success: true,
						responses: results
					});
				
				});
			
			});
		
		});
	
	});

});

/**
 * Fetches existing responses by session.
 */
$.app.handle('POST', '/response/resourceResponses', ['smile-session'], function() {

	// Session is required
	var session = this.session.data;
	var me = session.id_user;
	
	// Input is required
	var input = this.request.body;
	
	// Get the resourceIds and make sure it's an array
	var resourceIds = input.resourceIds;
	if(!resourceIds || !(resourceIds instanceof Array) || resourceIds.length < 1) {
		this.response.sendError(400, 2, 'Input validation error: ["resourceIds"]');
		return;
	}
	
	// Find the given resources
	this.couchDB.get('resource', 'by_id', { keys: resourceIds }, this, function(resources) {
	
		// Make sure all resources were found
		if(resources.length !== resourceIds.length) {
			this.response.sendError(404, 13, 'Unable to find one or more referenced ids.');
			return;
		}
		
		// Get all responses by resourceId
		this.couchDB.get('response', 'by_resourceId', { keys: resourceIds }, this, function(responses) {
		
			// Normalize all results
			var results = [];
			for (var i in responses) {
				results.push(this.sanitize(responses[i]));
			}
			
			// Send the responses
			this.response.send(200, {
				success: true,
				responses: results
			});
		
		});
	
	});

});
