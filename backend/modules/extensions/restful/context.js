/**
 * Copyright (C) SMILE Consortium 2013,
 * Developed by Neticle Portugal
 */
 
$.using(

	'system.string',
	'system.restful.context'

);

// Register the couchDB handle in the restfulContext prototype
$.restfulContext.prototype.couchDB = $.app.couchDB;

/**
 * Finds an existing record through the CouchDatabase handle, by doctype and
 * unique identifier.
 *
 * @param string doctype
 *	The value of "doctype" of the document to find.
 *
 * @param string id
 *	The ID of the document to find.
 *
 * @param object context
 *	The context to be applied to the callback function.
 *
 * @param function callback
 *	The function to handle the event completion.
 */
$.restfulContext.prototype.find = function() {

	// Load arguments according to length
	var doctype, id, context, callback;
	
	switch(arguments.length) {
	
		case 3:
			doctype = arguments[0];
			id = arguments[1];
			callback = arguments[2];
		break;
		
		case 4:
			doctype = arguments[0];
			id = arguments[1];
			context = arguments[2];
			callback = arguments[3];
		break;
		
		default:
			throw 'Invalid argument count.';
	
	}

	// Perform a request to CouchDB
	this.couchDB.get('generic', 'by_id_doctype', { key: [ id, doctype ] },
			this, function(records) {
		
		if(records.length === 0) {
		
			// Send the error response
			this.response.sendError(404, 13,
				'Unable to find "' + doctype + '" with UUID: "' + id + '"'
			);
		
			return;
			
		}
		
		callback.call(context, records[0]);
			
	});

};

/**
 * Sanitizes the given document, making it ready to be included in the response
 * back to the user.
 *
 * @param object document
 *	The document to be sanitzed.
 *
 * @return object
 *	The sanitized object.
 */
$.restfulContext.prototype.sanitize = function(document) {

	// Get the document property filters
	var filters = this.request.query['onlyFields'];
	
	// The resulting object
	var result = { 
		UUID: document._id 
	};
	
	for(var property in document) {
	
		// Escape reserved properties
		if(property[0] === '_' || property === 'doctype' || property === 'UUID') {
			continue;
		}
		
		// Escape unwanted properties
		if(filters && filters.indexOf(property) < 0) {
			continue;
		}
		
		result[property] = document[property];
	}
	
	return result;

};

/**
 * Filters the input document by removing reserved keywords.
 *
 * @param object input
 *	The document to be sanitzed.
 *
 * @return object
 *	The sanitized object.
 */
$.restfulContext.prototype.filter = function(input) {

	var document = {};
	
	if ('object' === typeof input) {
		
		for (var p in input) {
			
			var pname = p.toLowerCase();
		
			if (pname[0] === '_' || pname === 'uuid' || pname === 'doctype') {
				continue;
			}
			
			document[p] = input[p];
			
		}	
		
	}
	
	return document;

};

/**
 * Adds a recently uploaded file as an attachment to the given document.
 *
 * @param string id
 *	The recently created document id.
 *
 * @param string revision
 *	The recently created document revision id.
 *
 * @param HttpUploadedFile file
 *	The uploaded file instance.
 *
 * @param string name
 *	The name of the attachment to add. If not specified, the name of the
 *	file will be used as the identifier.
 */
$.restfulContext.prototype.setAttachment = function(id, revision, file, name) {

	$.debug('RestfulContext: attatching uploaded file to document ' + id);

	// Build the document meta data for the attachment request
	var document = { _id: id, _rev: revision };
	var name = name ? name : file.name;
	var type = file.type;
	var file = file.getPath();
	
	// Set the attachment.
	this.couchDB.setAttachment(document, name, type, file, function(success) {
	
		if(!success) {
			$.error('RestfulContext: failed to upload document attachment.');
		}
	
	});

};








