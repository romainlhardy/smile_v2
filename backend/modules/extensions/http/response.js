/**
 * Copyright (C) SMILE Consortium 2013,
 * Developed by Neticle Portugal
 */

$.using('system.http.response');

/*
 * HTTP access control (CORS) headers
 * to allow cross-origin requests
 */
$.httpResponse.prototype.headers = {
	'Access-Control-Allow-Origin': '*',
	'Access-Control-Allow-Credentials': 'true',
	'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE',
	'Access-Control-Allow-Headers': 'Content-Type'
};

/**
 * Sends an error response with the given status, code and message.
 *
 * @param int status
 *	The response HTTP status code.
 *
 * @param int code
 *	The unique error identifier, according to the SMILE api.
 *
 * @param string message
 *	A human readable error message.
 */
$.httpResponse.prototype.sendError = function(status, code, message) {

	return this.send(status, {
		success: false,
		error: {
			code: code,
			msg: message
		}
	});
	
};

/**
 * Sends an "OK" message with a 200 status code.
 */
$.httpResponse.prototype.sendOK = function() {

	return this.send(200, {
		success: true
	});
	
};

/**
 * Sends an "OK" message with a 200 status code and the given object UUID.
 *
 * @param string uuid
 *	The UUID to be included in the response.
 */
$.httpResponse.prototype.sendUUID = function(uuid) {

	return this.send(200, {
		success: true,
		UUID: uuid
	});

};

/**
 * Handles all OPTIONS requests by responding with a generic "OK" message
 * which also includes the CORS headers.
 */
$.app.handle('OPTIONS', '/%s', function() {
	this.response.sendOK();
});
