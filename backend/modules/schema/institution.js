/**
 * Copyright (C) SMILE Consortium 2013,
 * Developed by Neticle Portugal
 */

$.using(

	'system.validator'
	
);

module.exports = {

	// Module id
	_id: 'schemaInstitution',

	// Validator instance	
	validator: $.validator.create([
	
		// Unsafe attributes for all events
		['unsafe', 'creator,photo'],
	
		// Required attributes during POST
		['required', 'name, institutionType', 'POST']
	
	]),
	
	// Document template
	template: {
		doctype: 'institution',
	
		name: undefined,
		institutionType: "University",
		country: undefined,
		region: undefined,
		city: undefined,
		address: undefined,
		zip: undefined,
		creator: undefined,
		createdOn: undefined,
		website: "www.stanford.edu"
	}

};

