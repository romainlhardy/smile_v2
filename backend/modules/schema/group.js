/**
 * Copyright (C) SMILE Consortium 2013,
 * Developed by Neticle Portugal
 */

$.using(
	'system.validator'
	
);

module.exports = {

	// Module id
	_id: 'schemaGroup',

	// Validator instance	
	validator: $.validator.create([
	
		// Required attributes
		['required', 'name', 'POST'],
		
		['unique', 'name', undefined, { design: 'group', view: 'by_name' }],
		
		// Arrays
		['string-array', 'memberIds,organizerIds,tags,gradeLevels,subjects,invitedIds'],
		
		// Boolean
		['bool', 'public,requirePasscode']
		
	]),
	
	// Group template
	template: {
		doctype: 'group',
	
		name: undefined,
		groupType: undefined,
		institution: undefined,
		owner: undefined,
		createdOn: undefined,
		memberIds: [],
		organizerIds: [],
		"public": true,
		requirePasscode: false,
		passcode: undefined,
		invitedIds: undefined,
		tags: undefined,
		gradeLevels: undefined,
		subjects: undefined,
		language: "English"
	
	}
	
};
