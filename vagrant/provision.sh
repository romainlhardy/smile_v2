#!/usr/bin/env bash
SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")

#Load Design Documents into CouchDB
function load_couchdb_docs {
	echo "Loading CouchDB design docs"
	source $SCRIPTPATH/couch.sh
}

load_couchdb_docs